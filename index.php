<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Universidad UNIACC</title>
    <link rel="apple-touch-icon" sizes="57x57" href="assets/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/favicons/apple-icon-72x72.png">
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Universidad UNIACC</title>
        <link rel="apple-touch-icon" sizes="57x57" href="assets/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="assets/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/favicons/favicon-16x16.png">
        <!--<link rel="manifest" href="/manifest.json">-->
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:300,400,500,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
            crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/css/slick.css">
        <link rel="stylesheet" href="../assets/css/slick-theme.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>

<body>
    <!-- ACÁ TODO EL HTML -->
    <!-- a toTop, social-share, a-blog -->
    <div class="bg-light">
            <div class="container p-space bg-white px-5 d-flex justify-content-end">
                <a class="btn text-white btn-toTop">Arriba <i class="fas fa-chevron-up ml-2"></i></a>
            </div>
        </div>
        <div class="social-share p-3 rotate-social">
            <a href="#" class="text-decoration-none">
                <span class="fa-stack position-static m-social-icon">
                    <i class="fas fa-circle fa-stack-2x text-white"></i>
                    <i class="fab fa-facebook-f fa-stack-1x fa-inverse title-color"></i>
                </span>
            </a>
            <a href="#" class="text-decoration-none">
                <span class="fa-stack position-static m-social-icon">
                    <i class="fas fa-circle fa-stack-2x text-white"></i>
                    <i class="fab fa-instagram fa-stack-1x fa-inverse title-color"></i>
                </span>
            </a>
            <a href="#" class="text-decoration-none">
                <span class="fa-stack position-static m-social-icon">
                    <i class="fas fa-circle fa-stack-2x text-white"></i>
                    <i class="fab fa-twitter fa-stack-1x fa-inverse title-color"></i>
                </span>
            </a>
            <a href="#" class="text-decoration-none">
                <span class="fa-stack position-static m-social-icon">
                    <i class="fas fa-circle fa-stack-2x text-white"></i>
                    <i class="fab fa-linkedin-in fa-stack-1x fa-inverse title-color"></i>
                </span>
            </a>
            <a href="#" class="text-decoration-none ">
                <span class="fa-stack position-static m-social-icon">
                    <i class="fas fa-circle fa-stack-2x text-white"></i>
                    <i class="fab fa-youtube fa-stack-1x fa-inverse title-color"></i>
                </span>
            </a>
            <a href="#" class="text-decoration-none">
                <span class="fa-stack position-static m-social-icon">
                    <i class="fas fa-circle fa-stack-2x text-white"></i>
                    <i class="fab fa-google-plus-g fa-stack-1x fa-inverse title-color"></i>
                </span>
            </a>
        </div>
        <div class="rotate-blog">
            <a href="#" class="btn btn-danger p-blog a-blog text-size-body"><b>BLOG</b> UNIACC</a>
        </div>
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../assets/js/my-upload.js"></script>
        <script type="text/javascript" src="../assets/js/jquery.paroller.min.js"></script>
        <script type="text/javascript" src="../assets/js/slick.min.js"></script>
        <script type="text/javascript" src="../assets/js/app.js"></script>
    </body>
    
    </html>


